jQuery(document).ready(
    function () {
        jQuery('body').on(
            'DOMNodeInserted', '.cw_ffform', function () {
                cwforminit(this);
            }
        );
    }
);
jQuery('.cw_ffform').each(
    function () {
        cwforminit(this);
    }
);
function cwforminit(form)
{

    $form = jQuery(form);
    if ($form.hasClass('cwforminit')) { return;}
    $form.addClass('cwforminit');
    $formid = ($form.data('cwform'));
    jQuery.ajax(
        {
            url: cw_ff.ajaxurl+'?t='+Math.random(),
            type: 'POST',
            async: true,
            data: {
                action: 'cwfft'
            },
            success: function (out) {
                cwfft = out;
            }
        }
    );
    $form.find('input,select, textarea').keyup(
        function () {
            cwvalidate(jQuery(this));
        }
    );

    $form.find('[data-cwfilter]').each(
        function (e) {
            jQuery(this).keypress(
                function (e) {
                    var char = String.fromCharCode(e.which);
                    var txt = jQuery(this).val();
                    var filter = '' + jQuery(this).data('cwfilter') + '';
                    var regex = new RegExp(filter);
                    var now = txt + char;
                    if (!regex.test(now)) {
                        e.preventDefault();
                        return false;
                    }
                }
            );

        }
    );
    jQuery($form).find('[data-cwmessage]').each(
        function () {
            //    $input = jQuery(this);
            //    $input[0].setCustomValidity($input.data('cwmessage'));
        }
    );
    jQuery($form).find('[data-cwvalidate]').change(
        function () {
            cwvalidate(jQuery(this));
        }
    );
    $form.submit(
        function (e) {
            $thisform=jQuery(this);
            reply = cwformcollectdata(this);
            reply.append('action', 'cw_ff');
            reply.append('cwfft', cwfft);
            $thisform.find('[data-cwmessage]').each(
                function () {
                    cwvalidate(jQuery(this));
                }
            );
            e.preventDefault();
            $thisform.addClass('cwprocessing');
            jQuery.ajax(
                {
                    url: cw_ff.ajaxurl,
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: reply,
                    success: function (out) {
                        $thisform.removeClass('cwprocessing');
                        if (!out.success) {
                            if (out.data.type === 'field') {
                                $thisform.find('[name="' + out.data.details.cwname + '"]').addClass('error');
                            } else {
                                alert(out.data.details);
                            }
                        } else {
                            $thisform.find('*').css({visibility: 'hidden'});
                            $thisform.addClass('cwsubmitted');
                            formdata=false;
                            if ($thisform.is('[data-cwform1]')) {
                                formdata=($thisform.data('cwform1'));
                            }
                            if (formdata) {
                                        $thisform.before(formdata['success']);
                            }
                            jQuery().get($thisform.attr('action'));
                            jQuery('body').trigger('ngsubmitted');
                            console.log('ngsubmitted'+$thisform.attr('data-cwform'));
                            jQuery('body').trigger('ngsubmitted'+$formid);
                            if (typeof cwffsuccess === 'function') {
                                cwffsuccess($thisform);
                            }
                        }
                    }
                }
            );
        }
    );
}
function cwformcollectdata(formz)
{
    var data = new FormData();
    $forms = jQuery(formz);
    data.append('cwform', $forms.data('cwform'));
    $forms.find('input,textarea').each(
        function () {
            $this = jQuery(this);
            if ($this.is('[type="checkbox"]')) {
                if ($this.is(':checked')) {
                    data.append($this.attr('name'), $this.val());
                }
            } else {
                data.append($this.attr('name'), $this.val());
            }
        }
    );
    $forms.find('select').each(
        function () {
            $this = jQuery(this);
            data.append($this.attr('name'), $this.find('option:selected').text());
        }
    );
    $forms.find('input[type="file"]').each(
        function () {

            $this = jQuery(this);
            if ($this.val() != '') {
                data.append($this.attr('name'), $this[0].files[0]);
            }
        }
    );

    return data;
}
function cwvalidate($input)
{
    $input.removeClass('error');
    $input.removeAttr('title');
    input = $input.val();

    if (input === '') {
        return true;
    }
    var regex = new RegExp($input.data('cwvalidate'));
    if (!regex.test(input)) {
        $input.addClass('error');
        if ($input.data('cwmessage')) {
            if ($input.data('cwmessage')) {

                $input[0].setCustomValidity($input.data('cwmessage'));
            }
            return true;
        }
    } else {
        if ($input.data('cwmessage')) {
            $input[0].setCustomValidity("");

        }
        return false;
    }

}
