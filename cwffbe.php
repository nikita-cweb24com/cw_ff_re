<?php
/**
  Api Class

  PHP version 5.5.9
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
defined('ABSPATH') or die("No script kiddies please!");
/**
 * Our core class goes here
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
Class Cw_API_Endpoint
{

    /**
     * Hook WordPress
     *
     * @param string $endpoint point url part
     * @param object $callback method or function to call
     *
     * @return void
     */
    public function __construct($endpoint, $callback)
    {
        $this->point    = $endpoint;
        $this->callback = $callback;
        add_filter('query_vars', array($this, 'addQueryVars'), 0);
        add_action('parse_request', array($this, 'sniffRequests'), 0);
        add_action('init', array($this, 'addEndPoint'), 0);
    }

    /**
     * Add public query vars
     *
     * @param array $vars List of current public query vars
     * 
     * @return array $vars
     */
    public function addQueryVars($vars)
    {
        $vars[] = '__'.$this->point;
        //$vars[] = '';
        return $vars;
    }

    /**
     * Add API Endpoint
     *     This is where the magic happens - brush up on your regex skillz
     *
     * @return void
     */
    public function addEndPoint()
    {

        add_rewrite_rule(
            '^'.$this->point.'$/?', 'index.php?__'.$this->point.'=1', 'top'
        );
    }

    /**
     * Sniff Requests
     *     This is where we hijack all API requests
     *     If $_GET['__api'] is set, we kill WP and serve up pug bomb awesomeness
     *
     * @return die if API request
     */
    public function sniffRequests()
    {
        global $wp;
        if (isset($wp->query_vars['__'.$this->point])) {
            $this->handleRequest();
            exit;
        }
    }

    /**
     * Handle Requests
     *     This is where we send off for an intense pug bomb package
     *
     * @return void
     */
    protected function handleRequest()
    {
        $callback = $this->callback;
        $callback();
    }
}