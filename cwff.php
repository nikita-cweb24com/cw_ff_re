<?php
/**
  Plugin Name: cweb24 custom feedback form
  Plugin URI: http://cweb24.com
  Description: Allows to use custom form code.
 * Just include file with your <form></form> with shortcode
  Author: Nikita Menshutin
  Version: 3.2
  Author URI: http://cweb24.com

  PHP version 5.5.9
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Our core class goes here
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
class CwfbCore
{

    /**
     * Constructor
     */
    function __construct()
    {
        $this->prefix = Cwfb::prefix();
        if (!session_id()) {
            session_start();
        }
        $this->ver  = cwfb::version();
        $this->tags = array('input', 'select', 'textarea');
        $this->url  = $this->getUrl();
        $this->life = HOUR_IN_SECONDS * 12;
        $this->period=MINUTE_IN_SECONDS*5;
        $this->endPointUrl=get_site_url().'/'.$this->prefix;
        add_action('init', array($this, 'initSession'));
        add_action('init', array($this, 'initSubmissions'));
        add_action('init', array($this,'sendMailsDelayed'));
        add_shortcode($this->prefix, array($this, 'printform'));
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_action('wp_ajax_'.$this->prefix, array($this, 'ajax'));
        add_action('wp_ajax_nopriv_'.$this->prefix, array($this, 'ajax'));
        add_action('wp_ajax_cwfft', array($this, 'getToken'));
        add_action('wp_ajax_nopriv_cwfft', array($this, 'getToken'));

        new cw_API_Endpoint($this->prefix, array($this, 'sendMails'));
    }

    /**
     * Enqueing scripts to footer
     *
     * @return void
     */
    public function scripts()
    {
        wp_register_script(
            $this->prefix,
            plugin_dir_url(__FILE__).'cwff.js',
            array('jquery'),
            $this->ver,
            true
        );
        wp_localize_script(
            $this->prefix,
            $this->prefix,
            array(
            'ajaxurl' => str_replace(
                array(
                        'http://',
                        'https://'
                        ),
                '//',
                admin_url('admin-ajax.php')
            )
            )
        );
        wp_enqueue_script($this->prefix);
    }

    /**
     * Registering custom post types
     *
     * @return void
     */
    public function initSubmissions()
    {
        $labels = array(
            'name' => __('Submissions'),
            'singular_name' => __('Submission'),
            'add_new' => __('Add submission'),
            'all_items' => __('All submissions'),
            'add_new_item' => __('Add submission'),
            'edit_item' => __('Edit submission'),
            'new_item' => __('New submission'),
            'view_item' => __('View submission'),
            'search_items' => __('Search submission'),
            'not_found' => __('No submissions found'),
            'not_found_in_trash' => __('No submissions found in trash'),
            'parent_item_colon' => __('Parent submission')
        );
        $args   = array(
            'labels' => $labels,
            'public' => false,
            'show_ui' => true,
            'has_archive' => false,
            'show_in_menu' => true,
            'publicly_queryable' => false,
            'query_var' => true,
            'exclude_from_search' => true,
            'rewrite' => false,
            'capability_type' => 'post',
            'capability_type' => 'post',
            'capabilities' => array(
                'create_posts' => 'do_not_allow',
            ),
            'map_meta_cap' => true,
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor'
            )
        );
        register_post_type('submission', $args);
    }

    /**
     * Get token
     *
     * @return void
     */
    public function getToken()
    {
        $token=array_values(array_slice($this->tokens, -1));
        echo $token[0];
        wp_die();
    }

    /**
     * Method for ajax
     *
     * @return void (dies)
     */
    public function ajax()
    {
        $formdata      = $_POST[$this->prefix]['formdata'];
        $this->formurl = $_POST[$this->prefix]['url'];
        $this->form    = get_transient($formdata);
        $nonce         = $_POST[$this->prefix]['protect'];
        if (!$this->verify($nonce, $formdata)) {
            wp_die();
        }
        $token      = ($_POST['cwfft']);
        $validtoken = false;
        foreach ($this->tokens as $v) {
            if ($v == $token) {
                $validtoken = true;
            }
        }
        if (!$validtoken) {
            wp_die();
        }
        foreach ($this->form as $k => $v) {
            $this->form[$k]['value'] = $_POST[$v['cwname']];
        }
        if (isset($_FILES)&&!empty($_FILES)) {
            $this->formfiles=array();
            foreach ($_FILES as $k => $v) {
                if ($v['error']!=0) {
                            $msg=array('type'=>'file','details'=>'File error');
                            wp_send_json_error($msg);
                }
                $this->formfiles[$k]=$v;
            }
        }
        $this->formValidate();
        $this->saveSubmission();
        wp_die();
    }

    /**
     * Validating form, calling submethods if required
     *
     * @return void
     */
    public function formValidate()
    {
        // HTTP ORIGIN
        if (2 == 1) {
            if ($_SERVER['HTTP_ORIGIN'] != get_site_url()) {
                $msg = array(
                    'type' => 'origin',
                    'details' => __('Please try again')
                );
                wp_send_json_error($msg);
            }
        }
        foreach ($this->form as $k => $value) {
            //required
            if (isset($value['required']) && !strlen($value['value'])) {
                $msg = array('type' => 'field', 'details' => $value);
                wp_send_json_error($msg);
            }
            if (!strlen($value['value'])) {
                continue;
            }
            //regex
            $method = 'formValidate'.ucfirst($value['type']);
            if (method_exists($this, $method) && strlen($value['type'])) {
                $value['value'] = $value['value'];
                $this->{$method}($value);
            }
            if (isset($value['data-cwvalidate'])) {
                $this->formValidateRegex($value);
            }
        }
    }

    /**
     * Validating field value with regex
     *
     * @param array $value field data
     *
     * @return boolean valid or not (dies)
     */
    public function formValidateRegex($value)
    {
        if (!preg_match('/'.$value['data-cwvalidate'].'/', $value['value'])) {
            $msg = array('type' => 'field', 'details' => $value);
            wp_send_json_error($msg);
            return false;
        }
        return true;
    }

    /**
     * Validate email
     *
     * @param array $value field data
     *
     * @return boolean
     */
    public function formValidateEmail($value)
    {
        if (!filter_var($value['value'], FILTER_VALIDATE_EMAIL)) {
            $msg = array('type' => 'field', 'details' => $value);
            wp_send_json_error($msg);
            return false;
        }
        return true;
    }

    /**
     * Validating hash (made with protect method)
     *
     * @param string $code   hash
     * @param string $action text which should correspond to hash
     *
     * @return boolean valid or not
     */
    public function verify($code, $action)
    {
        if ($this->protect($action) == $code) {
            return true;
        }
        return false;
    }

    /**
     * Protect method
     *
     * @param string $data to protect
     *
     * @return string hash
     */
    public function protect($data)
    {
        return md5($data);
    }

    /**
     * Shortcode to print form
     *
     * @param type $atts array atts
     * @param type $form string form name
     *
     * @return string printed form
     */
    public function printform($atts, $form)
    {
        $this->hasform = true;
        $xmlprefix     = '<?xml encoding="utf-8" ?>';
        $formid        = crc32(time());
        ob_start();
        if (file_exists($form)) {
            include $form;
        } else {
            get_template_part($form);
        }
        $html          = ob_get_clean();
        if (!strlen($html)) {
            return $form.' '.__('not found');
        }
        $doc       = new DOMDocument('1.0', 'UTF-8');
        $doc->loadHTML(
            $xmlprefix.$html//,
            //            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        $nodes_    = array();
        $node      = array();
        $names     = array();
        $form      = $doc->getElementsByTagName('form')->item(0);
        $formname  = $form->getAttribute('name');
        $form->setAttribute('name', 'cw'.sanitize_title($formname));
        $formclass = ($form->getAttribute('class')).' '.$this->prefix.'form';
        $form->setAttribute('class', $formclass);
        $form->setAttribute('data-cwform', $formid);
        $form->setAttribute('method', 'get');
        $form->setAttribute('action', $this->endPointUrl);
        foreach ($this->tags as $tag) {
            $nodes_[] = $doc->getElementsByTagName($tag);
        }
        foreach ($nodes_ as $k => $v) {
            if (empty($v)) {
                continue;
            }
            foreach ($v as $node_) {
                foreach ($node_->attributes as $attr) {
                    $name                         = $node_->getAttribute('name');
                    $node[$name][$attr->nodeName] = $attr->nodeValue;
                }
                $node[$name]['cwname'] = $this->sanitize($name);
                $node_->setAttribute('name', $this->sanitize($name));
            }
        }
        $ser                         = $node;
        $formsign                    = md5($formname.$formid.$this->url);
        $node[$this->prefix]['name'] = $formname;
        set_transient($formsign, $node, HOUR_IN_SECONDS * 12);
        $el                          = array(
            'formdata' => $formsign,
            'protect' => $this->protect($formsign),
            'url' => $this->url,
        );
        foreach ($el as $k => $v) {
            $newnode = $doc->createElement('input');
            $newnode->setAttribute('type', 'hidden');
            $newnode->setAttribute('name', $this->prefix.'['.$k.']');
            $newnode->setAttribute('value', htmlentities($v));
            $form->appendChild($newnode);
        }
        $node=array();
        foreach ($doc->getElementsByTagName('label') as $node_) {
            if (empty($v)) {
                continue;
            }
            foreach ($node_->attributes as $attr) {
                $id=$node_->getAttribute('for');
                if ($id) {
                    continue;
                }
                $name                         = $node_->getAttribute('for');
                if (!$name || strlen($name)==0) {
                    continue;
                }
                $node[$name][$attr->nodeName] = $attr->nodeValue;
                $node_->setAttribute('for', $this->sanitize($name));
            }
        }
        return html_entity_decode(
            str_replace(
                $xmlprefix,
                '',
                preg_replace(
                    '~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i',
                    '',
                    $doc->saveHTML()
                )
            )
        );
    }

    /**
     * Init Session
     *
     * @return void
     */
    public function initSession()
    {
        $_SESSION[$this->prefix][time()] = md5(time().$this->prefix.'12');
        foreach ($_SESSION[$this->prefix] as $k => $v) {
            if ((time() - (int) $k) > $this->life) {
                unset($_SESSION[$this->prefix][$k]);
            }
        }
        $this->tokens   = ($_SESSION[$this->prefix]);
        session_write_close();
        $this->notify[] = get_option('admin_email');
        $this->notify   = apply_filters('cwffrecipients', $this->notify);
    }

    /**
     * Echo file contents
     *
     * @param string $file file
     *
     * @return void
     **/
    private function _giveFileContents($file)
    {
        $this->attach['name']='';
        $this->attach['checksum']='';
        $dir=$this->uploadDir(wp_upload_dir());
        if (!file_exists($dir['basedir'].$file)) {
            return false;
        }

        //    echo basename($file);die();
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".basename($file)."\"");
        echo readfile($dir['basedir'].$file);
        die();
    }

    /**
    Check rights validate md5 and let download formfiles

    @return bool
     **/
    private function _giveFile()
    {
        if (!current_user_can('manage_options')) {
            auth_redirect();
            return;
        }
        if ($_GET['c'] !=md5($this->prefix.$_GET['a'].$_GET['b'])) {
            return;
        }
        $file=$this->_createUploadDir().'/'.$_GET['b'].'.php';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$_GET['a']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        echo file_get_contents($file);
        die();
    }

    /**
     * Send emails after expiration
     * $this->period
     *
     * @return void
     */
    public function sendMailsDelayed()
    {
        if (!get_transient($this->prefix.'send')) {
            $this->sendMails();
            set_transient($this->prefix.'send', true, 30);
        }
    }

    /**
     * Send mails (called via endpoint)
     *
     * @return void dies
     */
    public function sendMails()
    {
        if (isset($_GET['file']) && isset($_GET['check'])) {
            if (md5(
                crc32(
                    (
                          $_GET['file']
                        ).$this->prefix
                )
            )==$_GET['check']
            ) {
                $this->_giveFileContents($_GET['file']);
            }

        }
        if (isset($_GET['a'])
            && isset($_GET['b'])
            && isset($_GET['c'])
        ) {
            $this->fileData=$_GET;
            $this->_giveFile();
        }
        $args=array(
                'post_type' => 'submission',
                'posts_per_page' => 5,
             //   'orderby' => 'rand',
                'meta_query' => array(
                    array(
                        'key' => $this->prefix.'sent',
                        'compare' => '==',
                        'value'=>'0'
                    ),
                )
            );


        $posts_=get_posts($args);


        if (empty($posts_)) {
            return;
        }
        add_filter(
            'wp_mail_content_type',
            create_function('', 'return "text/html;charset=utf-8";')
        );


        if (!isset($this->notify)) {
            $this->notify = array();
        }
        $ac = $this->prefix.'_pre_send';

        apply_filters($this->prefix.'pre_send', $posts_);

        do_action($this->prefix.'pre_send', $posts_);
        foreach ($posts_ as $pp) {
            $attachments=get_post_meta($pp->ID, $this->prefix.'attachments', true);

            foreach ($this->notify as $email) {
                $message = $pp->post_content;
                $mailresult=wp_mail(
                    $email,
                    $pp->post_title,
                    $message, false,
                    $attachments
                );
                if (!$mailresult
                ) {var_dump($mail);                  
                    return false;
                }
            }            
            update_post_meta($pp->ID, $this->prefix.'sent', 1);
        }
    }

    /**
     * Save submitted data in CPT
     *
     * @return void
     */
    public function saveSubmission()
    {
        $this->form['url']=$this->formurl;
        $this->form=apply_filters($this->prefix.'beforesave', $this->form);
        unset($this->form['url']);
        $message = '';
        foreach ($this->form as $k => $v) {
            if ($k == $this->prefix) {
                continue;
            }
            if ($v['type']=='file') {
                continue;
            }
            if (strlen($v['value'])>0) {
                $message.=$k.' : '.$v['value']."\n<hr/>\n";
            }
        }
        if (isset($this->formfiles)&& !empty($this->formfiles)) {
            $message.=$this->saveSubmissionFiles();
        }
        $message.='<br><br>';
        $message.='<a href="'.$this->formurl.'">';
        $message.=$this->formurl.'</a>';

        $post_ = array(
            'post_type' => 'submission',
            'post_content' => $message,
            'post_title' => 'Form submission ['.
            $this->form[$this->prefix]['name'].
            ']',
            'post_status' => 'publish'
        );
        $md5   = md5(serialize($post_));

        $prev = get_posts(
            array(
                'post_type' => 'submission',
                'post_status' => 'any',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => $this->prefix.'checksum',
                        'value' => $md5,
                        'compare' => '='
                    )
                )
            )
        );
        if (!empty($prev)) {
            wp_send_json_success();
        }
        $id = wp_insert_post($post_);
        update_post_meta($id, $this->prefix.'sent', '0');
        if (!$id) {
            $msg = array(
                'type' => 'local',
                'details' => 'Проблема сохранения формы');
            wp_send_json_error($msg);
        }
        if (isset($this->formfiles)&& !empty($this->formfiles)) {
            $attachments=array();
            foreach ($this->formfiles as $k=>$v) {
                $attachments[]=$v['attach'];
            }
            update_post_meta(
                $id,
                $this->prefix.'attachments',
                $attachments
            );
        }
        update_post_meta($id, $this->prefix.'checksum', $md5);
        update_post_meta($id, $this->prefix.'raw', $this->form);

        wp_send_json_success();
    }

    /**
    Save file submissions

    @return void
     **/
    public function saveSubmissionFiles()
    {
        $out='';

        $out.=__('Files').': ';
        $out.='<br>';

        foreach ($this->formfiles as $k=>$v) {
            $contents=file_get_contents(
                $v['tmp_name']
            );
            $this->formfiles[$k]['checksum']=md5(
                $contents
            );
            $this->attach=$this->formfiles[$k];
            $dir=$this->uploaddir(wp_upload_dir());
            $base=$v['size'];

            (move_uploaded_file($v['tmp_name'], $dir['path'].'/'.$v['name']));
            //$file=urlencode($dir['url'].'/'.$v['name']);
            $file=$this->formfiles[$k]['checksum'].'/'.$v['name'];
            $this->formfiles[$k]['attach']=($dir['path'].'/'.$v['name']);
            $get=add_query_arg(
                array(
                    'file'=>$file,
                    'check'=>md5(
                        crc32(
                            $file.$this->prefix
                        )
                    )
                    ),
                $this->endPointUrl
            );
            $out.='<a href="'.$get.'">';
            $out.=$v['name'].' '.$v['type'].' ';
            $out.=floor($v['size']/1024).' kb ';
            $out.='(';
            $out.=$this->formfiles[$k]['checksum'];
            $out.=')';
            $out.='</a>';
            $out.='<br>';



            //add_filter( 'upload_dir', array($this,'uploadDir') );

        }
        //remove_filter( 'upload_dir', array($this,'uploadDir') );
        return $out;
        return;
        $out='';
        $uploaddir=$this->_createUploadDir();
        foreach ($this->form as $k => $v) {
            $name[$v['cwname']]=$k;
        }
        $f=time();
        foreach ($this->formfiles as $k => $v) {
            $link=add_query_arg(
                array(
                'a'=>$v['name'],
                'b'=>$f,
                'c'=>md5($this->prefix.$v['name'].$f)
                ),
                $this->endPointUrl
            );
            $label=($name[$k]);
            $out.=$label.' : ';
            $out.='<a href="'.$link.'">'.$v['name'].'</a>';
            move_uploaded_file($v['tmp_name'], $uploaddir.$f.'.php');
            $out."\n";
            $out.='<hr>';
            //move
        }
        die();
        return $out;
    }

    /**
    Custom upload dir for attachments

    @param string $dir forlder for uploads
     *
    @return string folder
     **/
    public function uploadDir($dir)
    {
        $prefix=$this->prefix.'/'.$this->attach['checksum'];
        $deny='Deny from  all';

        file_put_contents($dir['basedir'].'/'.$this->prefix.'/.htaccess', $deny);

        $dir['basedir'].='/'.$prefix;
        $dir['baseurl'].='/'.$prefix;
        $dir['path']=$dir['basedir'];
        $dir['url']=$dir['baseurl'];
        $dir['subdir']='/'.$prefix;
        if (!is_dir($dir['path'])) {
            mkdir($dir['path']);
        }
        file_put_contents($dir['path'].'/.htaccess', $deny);
        return $dir;
    }

    /**
    Create custom upload folder if not created

    @return string path
     **/
    private function _createUploadDir()
    {
        $udir=wp_upload_dir();
        $dir=$udir['basedir'].'/'.$this->prefix;
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        file_put_contents($dir.'/.htaccess', "Deny from all\n");
        return $dir.'/';
    }

    /**
     * Sanitize param
     *
     * @param string $name to sanitize
     *
     * @return string sanitized value
     */
    public function sanitize($name)
    {
        $suffix = '';
        if (strpos($name, '[]')) {
            $suffix = '[]';
        }
        return $this->prefix.crc32($name).$suffix;
    }

    /**
     * Get current page url
     *
     * @return string url
     */
    public function getUrl()
    {
        return (isset($_SERVER['HTTPS']) ? "https" : "http").
            "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
}

/**
 * Our Abstract goes here
 * methods are used along the theme
 *
  @category Cweb24
  @package  Cweb24
  @author   Nikita Menshutin <nikita@cweb24.com>
  @license  http://cweb24.com commercial
  @link     http://cweb24.com
 * */
abstract class Cwfb
{

    /**
     * Plugin prefix
     *
     * @param string $suffix suffic
     *
     * @return string prefig
     */
    static function prefix($suffix = '')
    {
        return 'cw_ff'.$suffix;
    }

    static function version()
    {
        return '3.4';
    }
    
    /**
     * Get tags used in html5 forms
     *
     * @return array tags
     */
    static function tags()
    {
        return array('input', 'textarea', 'select');
    }

    /**
     * Print form in theme
     *
     * @param string $form file with form name
     *
     * @return void
     */
    static function form($form)
    {
        echo do_shortcode('['.self::prefix().']'.$form.'[/'.self::prefix().']');
    }

    /**
     * Validate attrs for form fields
     *
     * @param type $suffix string field type
     *
     * @return void
     */
    static function validate($suffix)
    {
        $rules=array();
        $rules['phone']='data-cwvalidate="^[0-9\-\+]{9,15}$"'.
            'data-cwmessage="'.__(
                "from 7 to 9 characters. Please ".
                'use only digits, plus, minus and arentheses '
            ).'"'.
            ' data-cwfilter="^\+?[0-9\-\(\)\ \+]+$"';
        $rules['email'] = 'data-cwvalidate=\'^(([^<>()\[\]\\.,;:'.
            '\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]'.
            '{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(('.
            '[a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$\' '.
            ' data-cwmessage="'.__("Wrong email address").'"';
        apply_filters('cwff_rules', $rules);
        echo $rules[$suffix];
    }

    /**
     * If wp_enqueue is not used, you can enqueue manually
     *
     * @return void
     */
    static function scriptTag()
    {
        echo '<script>';
        echo 'var '.self::prefix().'='.json_encode(
            array(
            'ajaxurl' => str_replace(
                array(
                        'http://',
                        'https://'
                        ),
                '//',
                admin_url('admin-ajax.php')
            )
            )
        );
        echo ";";
        echo '</script>';
        echo '<script src="';
        echo plugin_dir_url(__FILE__).'cwff.js?version='.self::version().'"></script>';
    }

    /**
     * Sanitize param
     *
     * @param string $name to sanitize
     *
     * @return string sanitized value
     */
    static function sanitize($name)
    {
        $suffix = '';
        if (strpos($name, '[]')) {
            $suffix = '[]';
        }
        return self::prefix().crc32($name).$suffix.'!';
    }
}
/**
 * Including api hook class
 *
 * @include api class *
 */
require_once dirname(__FILE__).'/cwffbe.php';

new CwfbCore();

